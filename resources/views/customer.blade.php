<!DOCTYPE html>
<html>
<head>
    <title>Customers</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
@include('layouts.app') 
<div class="container">
    <a class="btn btn-success" href="javascript:void(0)" id="createNewCustomer"> <i class="fa fa-plus-circle" aria-hidden="true"></i> New Customer</a><br><br>

    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>E-mail</th>
                <th>Mobile</th>
                <th>Profile</th>
                <th>Gender</th>
                <th>Hobbies</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th width="280px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
   
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">

                <form id="customerForm" name="customerForm" class="form-horizontal" enctype="multipart/form-data" method="post">
                   <input type="hidden" name="customer_id" id="customer_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
     
                    <div class="form-group">
                        <label class="col-sm-2 control-label">E-Mail</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-12">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" value="" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Mobile</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile Number" value="" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Profile</label>
                        <div class="col-sm-12">
                            <img style="margin-left:25px;border-radius: 50%;" src="/storage/app/public/admin.png" width="40" height="40" />
                            <input type="file" name="image" placeholder="Choose image" id="image">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Gender</label>


                        <div class="col-sm-12">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" id="gender" value="Male">Male
                                </label>
                                <label>
                                    <input type="radio" name="gender" id="gender" value="Female">Female
                                </label>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hobbies</label>
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="hobbies" id="hobbies" value="Cricket">Cricket
                                </label>
                                <label>
                                    <input type="checkbox" name="hobbies" id="hobbies" value="Bike Ride">Bike Ride
                                </label>
                                <label>
                                    <input type="checkbox" name="hobbies" id="hobbies" value="Book Reading">Book Reading
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="" required="">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">City</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="city" name="city" placeholder="Enter City" value="" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">State</label>
                        <div class="col-sm-12">
                            <select name="state" id="state">
                                <option value="" selected disabled>Choose here</option>
                                <option value="TamilNadu">Tamil Nadu</option>
                                <option value="Karanataka">Karanataka</option>
                                <option value="Hyderabad">Hyderabad</option>
                            </select>
                        </div>
                    </div>
                    
      
                    <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Submit
                     </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
</body>
    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('customers.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
           // {data: 'password', name: 'password'},
            {data: 'mobile', name: 'mobile'},
            {data: 'image', name: 'image'},
            {data: 'gender', name: 'gender'},
            {data: 'hobbies', name: 'hobbies'},
            {data: 'address', name: 'address'},
            {data: 'city', name: 'city'},
            {data: 'state', name: 'state'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewCustomer').click(function () {
        $('#saveBtn').val("create-customer");
        $('#customer_id').val('');
        $('#customerForm').trigger("reset");
        $('#modelHeading').html("Create New Customer");
        $('#ajaxModel').modal('show');
    });

    (function($)  {
    $.fn.extend({
        check : function()  {
            return this.filter(":radio, :checkbox").attr("checked", true);
        },
        uncheck : function()  {
            return this.filter(":radio, :checkbox").removeAttr("checked");
        }
    });
    }(jQuery));
    
    $('body').on('click', '.editCustomer', function () {
      var customer_id = $(this).data('id');
      
      $.get("{{ route('customers.index') }}" +'/' + customer_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Customer");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#customer_id').val(data.id);
          $('#name').val(data.name);
          $('#email').val(data.email);
          $('#password').val(data.password);
          $('#mobile').val(data.mobile);
          $('#image').val(data.image);
          $('#gender').val(data.gender);
          $('#hobbies').val(data.hobbies);
          $('#address').val(data.address);
          $('#city').val(data.city);
          $('#state').val(data.state);
      })
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#customerForm').serialize(),
          url: "{{ route('customers.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#customerForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });
    
    $('body').on('click', '.deleteCustomer', function () {
     
        var customer_id = $(this).data("id");
        confirm("Are You sure want to delete !");
      
        $.ajax({
            type: "DELETE",
            url: "{{ route('customers.store') }}"+'/'+customer_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
     
  });
</script>
</html>